﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Movies_UWP.ViewModel.Commands
{
    public class AddMovieCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public MoviesViewModel ViewModel { get; set; }
        public AddMovieCommand(MoviesViewModel vm)
        {
            ViewModel = vm;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ViewModel.AddMovie();
        }
    }
}
