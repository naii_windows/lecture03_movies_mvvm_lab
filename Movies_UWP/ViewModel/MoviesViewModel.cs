﻿using Movies_UWP.Model;
using Movies_UWP.ViewModel.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies_UWP.ViewModel
{
    public class MoviesViewModel
    {
        public ObservableCollection<Movie> Movies { get; set; }
        public AddMovieCommand AddCommand { get; set; }

        public MoviesViewModel()
        {
            Movies = new ObservableCollection<Movie>(DummyDataSource.Movies);
            AddCommand = new AddMovieCommand(this);
        }

        public void AddMovie()
        {
            Movies.Add(new Movie() { Title = "Star Wars", Director = "J.J. Abramhs", Description = "In a Galaxy far far away...." });
        }

    }
}
